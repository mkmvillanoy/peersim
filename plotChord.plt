#!/usr/local/bin/gnuplot

if (!exists("filename")) filename='graph.dat'

unset key
set terminal png size 720,720
set output "chord.png"
set size square
set title "Chord topology" 
unset border
unset xtics
unset ytics



# this is for plotting the logical topology (using LinkObserver)
plot filename with lines lc rgb "#0091ea" ,\
     '' u ($1):($2):($3) with labels point pt 7 offset char 0,0.5 lc rgb "black"


pause mouse close
